package cmsc433.p2;

import java.util.List;

/**
 * Eaters are simulation actors that have two fields: a name, and a list of Food
 * items that constitue the Eater's order. When running, an eater attempts to
 * enter the restaurant (only successful if the restaurant has a free table),
 * place its order, and then leave the restaurant when the order is complete.
 */
public class Eater implements Runnable
{
    private final String name;
    private final List<Food> order;
    private final int orderNum;
    private static int cnt = 0;

    /**
     * You can feel free modify this constructor. It must take at least the name
     * and order but may take other parameters if you would find adding them
     * useful.
     */
    public Eater(String name, List<Food> order)
    {
        this.name = name;
        this.order = order;
        this.orderNum = ++cnt;
    }

    public String toString()
    {
        return name;
    }

    /**
     * This method defines what an Eater does: The eater attempts to enter the
     * restaurant (only successful if the restaurant has a free table), place
     * its order, and then leave the restaurant when the order is complete.
     */
    public void run()
    {
        // TODO: Implement
    }
}