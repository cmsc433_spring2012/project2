package cmsc433.p2;

/**
 * Cooks are simulation actors that have at least one field, a name. When
 * running, a cook attempts to retrieve outstanding orders placed by Eaters and
 * process them.
 */
public class Cook implements Runnable
{
    private final String name;

    /**
     * You can feel free modify this constructor. It must take at least the
     * name, but may take other parameters if you would find adding them useful.
     * 
     * @param: the name of the cook
     */
    public Cook(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return name;
    }

    /**
     * This method executes as follows. The cook tries to retrieve orders placed
     * by Eaters. For each order, a List<Food>, the cook submits each Food item
     * in the List to an appropriate Machine, by calling makeFood(). Once all
     * machines have produced the desired Food, the order is complete, and the
     * Eater is notified. The cook can then go to process the next order. If
     * during its execution the cook is interrupted (i.e., some other thread
     * calls the interrupt() method on it, which could raise
     * InterruptedException if the cook is blocking), then it terminates.
     */
    public void run()
    {
        // TODO: Implement
    }
}