package cmsc433.p2;

import java.util.List;

/**
 * Simulation is the main class used to run the simulation. You may add any
 * fields (static or instance) or any methods you wish.
 */
public class Simulation
{

    // we suggest you implement this for use by other classes in
    // the simulation to log events
    public static void logEvent(SimulationEvent event)
    {
    }

    /**
     * Function responsible for performing the simulation. Returns a List of
     * SimulationEvent objects, constructed any way you see fit. This List will
     * be validated by a call to Validate.validateSimulation. This method is
     * called from Simulation.main(). We should be able to test your code by
     * only calling runSimulation.
     * 
     * @param numEaters
     *            the number of eaters wanting to enter the restaurant
     * @param numCooks
     *            the number of cooks in the simulation
     * @param numTables
     *            the number of tables in the restaurant (i.e. restaurant
     *            capacity)
     * @param machineCapacity
     *            the capicity of all machines in the restaurant
     */
    public static List<SimulationEvent> runSimulation(int numEaters,
            int numCooks, int numTables, int machineCapacity)
    {
        // TODO: Add all simulation code here, returning a List of
        // SimulationEvent objects
        // You may construct the list in any way you see fit. We recommend using
        // the Simulation.logEvent function.

        return null;
    }

    /**
     * Entry point for the simulation. All simulation code, however, should be
     * in runSimulation, so that we can test your simulation by only calling
     * runSimulation() then Validate.validateSimulation. This means that most
     * code from your original Simulation.main should probably now be in
     * Simulation.runSimulation.
     * 
     * @param args
     *            the command-line arguments for the simulation. There should be
     *            exactly four arguments: the first is the number of eaters, the
     *            second is the number of cooks, the third is the number of
     *            tables in the restaurant, and the fourth is the number of
     *            items each cooking machine can make at the same time.
     */
    public static void main(String args[]) throws InterruptedException
    {
        // Parameters to the simulation
        if (args.length != 4)
        {
            System.err
                    .println("usage: java Simulation <#eaters> <#cooks> <#tables> <capacity>");
            System.exit(1);
        }
        int numEaters = new Integer(args[0]).intValue();
        int numCooks = new Integer(args[1]).intValue();
        int numTables = new Integer(args[2]).intValue();
        int machineCapacity = new Integer(args[3]).intValue();

        List<SimulationEvent> simEvents;

        // Run the simulation
        simEvents = runSimulation(numEaters, numCooks, numTables,
                machineCapacity);

        // Validate the simulation
        Validate.validateSimulation(simEvents);
    }
}
