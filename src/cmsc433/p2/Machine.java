package cmsc433.p2;

import java.util.Random;

/**
 * A Machine is used to make a particular Food. Each Machine makes just one kind
 * of Food. Each machine has a capacity: it can make that many food items in
 * parallel; if the machine is asked to produce a food item beyond its capacity,
 * the requester blocks. Each food item takes at least item.cookTimeMS
 * milliseconds to produce.
 */
public class Machine
{
    public final String name;
    public final Food food;

    
    // Returns cook time for a given food 
    private int CookTime(Food food){
    	Random generator = new Random();
    	int noise=-1;
    	do{
    	  noise = (int)(20.0*generator.nextGaussian());	
    	}while ( food.cookTimeMS+noise<=0 );
    		  
    	int totalcooktime=food.cookTimeMS+noise;
    	return totalcooktime;
    }
    
    /**
     * The constructor takes at least the name of the machine, the Food item it
     * makes, and its capacity. You may extend it with other arguments, if you
     * wish. Notice that the constructor currently does nothing with the
     * capacity; you must add code to make use of this field (and do whatever
     * initialization etc. you need).
     */
    public Machine(String name, Food food, int capacity)
    {
        this.name = name;
        this.food = food;
    }

    /**
     * This method is called by a Cook in order to make the Machine's food item.
     * You can extend this method however you like, e.g., you can have it take
     * extra parameters or return something other than void. It should block if
     * the machine is currently at full capacity. If not, the method should
     * return, so the Cook making the call can proceed. You will need to
     * implement some means to notify the calling Cook when the food item is
     * finished.
     */
    public void makeFood() throws InterruptedException
    {
        // TODO: Implement
    }

    public String toString()
    {
        return name;
    }
}